package config;

import model.Department;
import model.Employee;
import model.PersonalDetails;
import model.Project;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class SessionManager extends AbstractSessionManager {
    private static final SessionManager SESSION_MANAGER = new SessionManager();


    @Override
    protected void setAnnotatedClasses(Configuration configuration) {

       configuration.addAnnotatedClass(Employee.class);
       configuration.addAnnotatedClass(Department.class);
       configuration.addAnnotatedClass(Project.class);
       configuration.addAnnotatedClass(PersonalDetails.class);


    }
    public static SessionFactory getSessionFactory(){
        return SESSION_MANAGER.getSessionFactory("practice");
    }
    public static void shutDown (){
        SESSION_MANAGER.shutdownSessionManager();
    }
}
