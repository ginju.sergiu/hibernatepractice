package repository;

import config.SessionManager;
import model.Department;
import model.Employee;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class DepartmentRepository {
    public void create(Department department) {
        Transaction transaction = null;
        try {
            Session session = SessionManager.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.save(department);
            transaction.commit();
            session.close();

        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }

    public Department getById(int id) {
        Session session = SessionManager.getSessionFactory().openSession();
        Department department = session.find(Department.class, id);
        session.close();
        return department;
    }

    public Department getByIdWithEmployeesLoaded(int id) {
        Session session = SessionManager.getSessionFactory().openSession();
        Department department = session.find(Department.class, id);
        for (Employee employee : department.getEmployees()) {
            System.out.println(employee);
        }
        session.close();
        return department;
    }
}
