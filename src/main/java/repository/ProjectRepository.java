package repository;

import config.SessionManager;
import model.Project;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class ProjectRepository {
    public void create(Project project){
        Transaction transaction = null;
        try{
            Session session = SessionManager.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.save(project);
            transaction.commit();
            session.close();
        }catch(Exception e){
            e.printStackTrace();
            if(transaction != null){
                transaction.rollback();
            }
        }
    }
    public Project getById(int id){
        Session session = SessionManager.getSessionFactory().openSession();
        Project project = session.find(Project.class,id);
        session.close();
        return project;
    }
}
