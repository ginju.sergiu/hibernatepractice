package repository;

import config.SessionManager;
import model.Employee;
import model.PersonalDetails;
import model.Project;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class EmployeeRepository {

    public void create(Employee employee) {
        Transaction transaction = null;
        try {
            Session session = SessionManager.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.save(employee);
            transaction.commit();
            session.close();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    public void remove(Employee employee) {
        Transaction transaction = null;
        try {
            Session session = SessionManager.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.remove(employee);
            transaction.commit();
            session.close();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    public void update(Employee employee) {
        Transaction transaction = null;
        try {
            Session session = SessionManager.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.update(employee);
            transaction.commit();
            session.close();

        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }
    public void save(String employeeLastName){
        Transaction  transaction = null;
        try{
            Session session = SessionManager.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            Employee employee = new Employee();
            employee.setLastName(employeeLastName);
            session.save(employee);
            transaction.commit();
            System.out.println(employee);
            session.close();
        }catch (Exception e){
            if(transaction != null){
                transaction.rollback();
            }
        }

    }

    public List<Employee> getAll() {
        List<Employee> employees = new ArrayList<Employee>();
        try {
            Session session = SessionManager.getSessionFactory().openSession();
            employees = session.createQuery("from Employee", Employee.class).list();
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return employees;
    }

    public Employee getById(int id) {
        Employee employee = null;
        try {
            Session session = SessionManager.getSessionFactory().openSession();
            employee = session.find(Employee.class, id);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return employee;
    }

    public List<Employee> getByFirstName(String firstName) {
        List<Employee> list  = null;
        try{
            Session session = SessionManager.getSessionFactory().openSession();
            Query<Employee> employeeQuery = session.createQuery("from Employee e where e.firstName like : firstName",Employee.class);
            employeeQuery.setParameter("firstName",firstName);
            list = employeeQuery.list();
            for(Employee employee: list){
                System.err.print(employee.getFirstName());
            }
        }catch(Exception e){
          e.printStackTrace();
        }
        return list;
    }

    public void updateEmployeeProjects(Integer id, Set<Project>projects){
        Transaction transaction = null;
        try{
            Session session = SessionManager.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            Employee employee = session.find(Employee.class,id);
            if(employee != null){
                employee.setProjects(projects);
                session.update(employee);
            }
            transaction.commit();
            session.close();

        }catch(Exception e){
            if(transaction != null){
                transaction.rollback();
            }

        }
    }
    public Employee findByIdWithProjectsLoaded(int id){
        Session session = SessionManager.getSessionFactory().openSession();
        Employee employee = session.find(Employee.class,id);
       for(Project project : employee.getProjects()){
           System.out.println(employee);
       }
       session.close();
       return employee;
    }

    public void create1(Employee employee, PersonalDetails personalDetails) {
        Transaction transaction = null;
        try {
            Session session = SessionManager.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.save(personalDetails);
            employee.setPersonalDetails(personalDetails);
            session.save(employee);

            transaction.commit();

        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        }

    }


    @SuppressWarnings("unchecked")
    public List<Employee> getAllEmployessWithSpecificNrOfChildren(int nrOfChildren){
        Session session = SessionManager.getSessionFactory().openSession();
        String hQLQuery = "from Employee e where e.personalDetails.numberOfChildren= :nrChildren";
        Query<Employee> query = session.createQuery(hQLQuery);
        query.setParameter("nrChildren",nrOfChildren);
        List<Employee>employees = query.list();
        session.close();
        return  employees;
    }

    @SuppressWarnings("unchecked")
    public List<Employee> getAllEmployeesStartingWith(String prefix){
        Session session = SessionManager.getSessionFactory().openSession();
        String hQLQuery = "from Employee e where e.firstName like :pre";
        Query<Employee> query = session.createQuery(hQLQuery);
        query.setParameter("pre",prefix+"%");
        List<Employee>employees = query.list();
        session.close();
        return employees;
    }

    @SuppressWarnings("unchecked")
    public List<Employee> getAllEmployeesOrderedAlphabetically(){
        Session session =SessionManager.getSessionFactory().openSession();
        String hQLQuery = "from Employee e order by e.lastName asc";
        Query<Employee>query = session.createQuery(hQLQuery);
        List<Employee>employees = query.list();
        session.close();
        return employees;
    }

}
