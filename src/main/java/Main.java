import config.SessionManager;
import model.*;
import repository.DepartmentRepository;
import repository.EmployeeRepository;
import repository.ProjectRepository;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Employee employee1 = new Employee("Stici", "Nicolae", CompanyRole.CEO);
        Employee employee2 = new Employee("Ginju", "Eusebiu", CompanyRole.CTO);
        Employee employee3 = new Employee("Andrian", "Elodia", CompanyRole.DCC);
        EmployeeRepository employeeRepository = new EmployeeRepository();
        employeeRepository.create(employee1);
        employeeRepository.create(employee2);
        employeeRepository.create(employee3);
        employeeRepository.remove(employee1);
        employeeRepository.getById(2);
        employeeRepository.update(employee2);
        System.out.println(employeeRepository.getByFirstName("Eusebiu"));
        employeeRepository.getAll();



        employeeRepository.save("Sergiu");
        Project project1 = new Project("Alfa", "Greaca", new Date());
        Project project2 = new Project("Bet", "Greaca", new Date());
        Project project3 = new Project("Omega", "Greaca", new Date());
        ProjectRepository projectRepository = new ProjectRepository();
        projectRepository.create(project1);
        projectRepository.create(project2);
        projectRepository.create(project3);
        Set<Project> projects = new HashSet<>();
        projects.add(project1);
        projects.add(project2);
        projects.add(project3);
        employeeRepository.updateEmployeeProjects(2, projects);
        Department department1 = new Department("HR", "Human Resource");
        DepartmentRepository departmentRepository = new DepartmentRepository();
        departmentRepository.create(department1);
        departmentRepository.getById(1);
        departmentRepository.getByIdWithEmployeesLoaded(1);
        System.out.println(employeeRepository.getByFirstName("Nicolae"));
        System.out.println(employeeRepository.getAllEmployeesStartingWith("Nico"));
        System.out.println(employeeRepository.getAllEmployessWithSpecificNrOfChildren(1));

        System.out.println(employeeRepository.getAllEmployeesOrderedAlphabetically());
        employeeRepository.findByIdWithProjectsLoaded(2);

         PersonalDetails personalDetails = new PersonalDetails("11919199191","bucuresti",true,1);

         Employee employee4 = new Employee("Eu","Filip",CompanyRole.DCC);
         employeeRepository.create1(employee4,personalDetails);

        SessionManager.shutDown();

    }
}
