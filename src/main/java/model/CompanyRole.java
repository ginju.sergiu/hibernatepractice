package model;

public enum CompanyRole {
    CEO,
    CTO,
    REG,
    ISS,
    DCC;
}
