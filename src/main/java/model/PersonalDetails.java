package model;

import javax.persistence.*;

@Entity
@Table(name = "personal_details")

public class PersonalDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer id;
    @Column
    private String personalNumber;
    @Column
    private String adress;
    @Column
    private Boolean isMarried;
    @Column
    private Integer numberOfChildren;

    @OneToOne(mappedBy = "personalDetails")
    private Employee employee;

    public PersonalDetails() {
    }

    public PersonalDetails(String personalNumber, String adress, Boolean isMarried, Integer numberOfChildren) {
        this.personalNumber = personalNumber;
        this.adress = adress;
        this.isMarried = isMarried;
        this.numberOfChildren = numberOfChildren;
        this.employee = employee;
    }

    public String getPersonalNumber() {
        return personalNumber;
    }

    public void setPersonalNumber(String personalNumber) {
        this.personalNumber = personalNumber;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public Boolean getMarried() {
        return isMarried;
    }

    public void setMarried(Boolean married) {
        isMarried = married;
    }

    public Integer getNumberOfChildren() {
        return numberOfChildren;
    }

    public void setNumberOfChildren(Integer numberOfChildren) {
        this.numberOfChildren = numberOfChildren;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }


    @Override
    public String toString() {
        return "PersonalDetails{" +
                "id=" + id +
                ", personalNumber='" + personalNumber + '\'' +
                ", adress='" + adress + '\'' +
                ", isMarried=" + isMarried +
                ", numberOfChildren=" + numberOfChildren +
                ", employee=" + employee +
                '}';
    }
}
